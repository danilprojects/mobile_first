import React from 'react'

import Calendar from './Calendar'
import FirstScreen from './FirstScreen'

class Home extends React.Component {
    constructor(props) {
        super(props)

        this.hendleClick = this.hendleClick.bind(this)
    }

    hendleClick() {
        console.log('click')
    }

    render() {
        return(
            <>
                <FirstScreen></FirstScreen>
                <Calendar click={this.hendleClick}></Calendar>
            </>
        )
    }
}
export default Home;