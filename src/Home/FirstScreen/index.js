import React from 'react'
import televisor from '../../image/televisor.png'

import style from './FirstScreen.module.scss'

const FirstScreen = (props) => {
    return (
        <section>
            <div className={style.block}> 
                <img src={televisor} alt='televisor'/>
                <p>
                    Для получения списка сериалов, пожалуйста, выберите необходимый месяц и день.
                </p>
            </div>
        </section>
    )
}

export default FirstScreen