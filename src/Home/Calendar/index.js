import React from 'react';

import style from './Calendar.module.scss'

import Day from './Day'

class Calendar extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        days: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        months: ['Январь' , 'Февраль' , 'Март' , 'Апрель' , 'Май' , 'Июнь' , 'Июль' , 'Август' , 'Сентябрь' , 'Октябрь' , 'Ноябрь' , 'Декабрь'],
        lastMonth: 11,
        month: 0,
        nextMonth: 1,
        year: 0,
        currentMonth: 0,
        currentYear: 0,
        calendar: [
          { id: 'week-1', data: [0, 0, 0, 0, 0, 0, 0] },
          { id: 'week-2', data: [0, 0, 0, 0, 0, 0, 0] },
          { id: 'week-3', data: [0, 0, 0, 0, 0, 0, 0] },
          { id: 'week-4', data: [0, 0, 0, 0, 0, 0, 0] },
          { id: 'week-5', data: [0, 0, 0, 0, 0, 0, 0] }
        ],
        holidays: [],
        holiday: '',
        isActive: false
      };
  
      this.previousCalendar = this.previousCalendar.bind(this);
      this.nextCalendar = this.nextCalendar.bind(this);

    }
  
    componentWillMount() {
      const now = new Date();
      const currentMonth = now.getMonth();
      const currentYear = now.getFullYear();
  
      this.setState({
        currentMonth,
        currentYear,
        month: currentMonth,
        year: currentYear,
      });
  
      this.setCalendar(new Date(currentYear, currentMonth, 1));
    }
  
    setMonth(date) {
      const month = date.getMonth();
      const lastMonth = month === 0 ? 11 : month - 1;
      const nextMonth = month === 11 ? 0 : month + 1;
  
      this.setState({
        lastMonth,
        month,
        nextMonth,
      });
  
      return { lastMonth, month, nextMonth };
    }
  
    setCalendar(date) {
      const { lastMonth, month, nextMonth } = this.setMonth(date);
      const year = date.getFullYear();
      const weekday = date.getDay();
      const days = this.checkLeapYear(year);
      let nextMonthDay = 0;
  
      const firstWeek = this.state.calendar[0].data.map((day, index) => {
        let holiday = '';
        if (index < weekday) {
          const value = (days[lastMonth] - (weekday - index)) + 1;
          return {
            value,
            class: style.day__soft,
            month: lastMonth,
          };
        }
        const value = (index - weekday) + 1;
        return {
          value: (index - weekday) + 1,
          class: '',
          month,
        };
      });
      const secondWeek = this.state.calendar[0].data.map((day, index) => {
        const value = firstWeek[6].value + index + 1;
        return {
          value,
          class: '',
          month,
        };
      });
      const thirdWeek = this.state.calendar[0].data.map((day, index) => {
        const value = secondWeek[6].value + index + 1;
        return {
          value,
          class: '',
          month,
        };
      });
      const forthWeek = this.state.calendar[0].data.map((day, index) => {
        const value = thirdWeek[6].value + index + 1;
        return {
          value,
          class: '',
          month,
        };
      });
      const fifthWeek = this.state.calendar[0].data.map((day, index) => {
        if (forthWeek[6].value + index + 1 > days[month]) {
          nextMonthDay += 1;
          return {
            value: nextMonthDay,
            class: style.day__soft,
            month: nextMonth,
          };
        }
        const value = forthWeek[6].value + index + 1;
        return {
          value,
          class: '',
          month,
        };
      });
  
      this.setState({
        month,
        year,
        calendar: [
          { id: 'week-1', data: firstWeek },
          { id: 'week-2', data: secondWeek },
          { id: 'week-3', data: thirdWeek },
          { id: 'week-4', data: forthWeek },
          { id: 'week-5', data: fifthWeek },
        ],
      });
    }
  
    checkLeapYear(year) {
      let days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
        days = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      }
      this.setState({
        days,
      });
      return days;
    }
  
    previousCalendar() {
      const month = this.state.month !== 0 ? this.state.month - 1 : 11;
      const year = this.state.month !== 0 ? this.state.year : this.state.year - 1;
      this.setCalendar(new Date(year, month, 1));
    }
  
    nextCalendar() {
      const month = this.state.month !== 11 ? this.state.month + 1 : 0;
      const year = this.state.month !== 11 ? this.state.year : this.state.year + 1;
      this.setCalendar(new Date(year, month, 1));
    }
  
    render() {
      //console.log(this.state.calendar);
      return (
        <section className={style.calendar}>
          <div className={style.calendar_header}>
            <span className={`${style.button_container} ${style.button_container__left}`}>
              <button onClick={this.previousCalendar} className={`${style.button_content} ${style.button_content__left}`} />
            </span>
            <span className="calendar-header-date">{`${this.state.months[this.state.month]}`}</span>
            <span className={`${style.button_container} ${style.button_container__right}`}>
              <button onClick={this.nextCalendar} className={`${style.button_content} ${style.button_content__right}`} />
            </span>
          </div>
          {this.state.calendar.map(week =>
            <div key={week.id} className={style.week}>
              {week.data.map(day =>
                <Day key={`${day.month}${day.value}`} classes={`${style.day} ${day.class}`} activeClass={style.active} data={{month: day.month + 1, day: day.value}} day={day} />
              )}
            </div>
          )}
        </section>
      );
    }
  }

export default Calendar;