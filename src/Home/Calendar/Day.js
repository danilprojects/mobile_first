import React from 'react'

import { connect } from 'react-redux';

import { getSeriesAction as getSeriesFromApi} from '../../action/getSeriesAction'

class Day extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isActive: false
        }
        
        this.getDateClick = this.getDateClick.bind(this);
    }
    getDateClick(e) {
        let date = arguments[0];

        let replaceDate = ({month, day}) => {
            if(month < 10 && day < 10) {
                return `0${month}-0${day}`;
            } else if (month < 10) {
                return `0${month}-${day}`;
            } else {
                return `${month}-${day}`;
            }
        } 

        let currentDate = `2019-${replaceDate(date)}`
  
        this.setState({
          isActive: !this.state.isActive
        })

        this.props.getSeriesFromApi(currentDate)

      }
    render() {
        return (
            <div
            className={`${this.props.classes} ${this.state.isActive ? this.props.activeClass : ''}`}
            onClick={this.getDateClick.bind(this, this.props.data)}
            >{this.props.day.value < 10 && this.props.day.value !== ' ' ? `0${this.props.day.value}` : this.props.day.value}</div>
        )
    }
}

const mapDispatchToProps = {
    getSeriesFromApi
}

export default connect(null, mapDispatchToProps)(Day);