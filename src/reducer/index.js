import { combineReducers } from 'redux'

import getSeries from './getSeries'

const rootReducer = combineReducers({
    getSeries
})

export default rootReducer;