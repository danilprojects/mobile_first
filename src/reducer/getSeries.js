import {
    GET_SERIES_START,
    GET_SERIES_SUCCESS,
    GET_SERIES_FAIL
} from '../action/constant.js'

const initialState = {
    statusLoad: {
        isLoad: false,
        isFail: false
    },
    payload: []
}

const getSeries = (state = initialState, {action, payload}) => {
    switch (action) {
        case GET_SERIES_START:
            return {
                ...state,
                statusLoad: {
                    isLoad: true,
                    isFail: false
                },
                payload
            }
        case GET_SERIES_SUCCESS:
            return {
                ...state,
                statusLoad: {
                    isLoad: false,
                    isFail: false
                },
                payload
            }
        case GET_SERIES_FAIL:
            return {
                ...state,
                statusLoad: {
                    isLoad: false,
                    isFail: true
                },
                payload
            }  
        default:
            return state;
    }
}

export default getSeries;