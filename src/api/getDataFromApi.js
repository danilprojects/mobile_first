import axios from 'axios';

export class getDataFromApi {
    async getData(date) {
        const response = axios.get(`http://api.tvmaze.com/schedule?country=US&date=${date}`)
        .then(res => res.data);
        
        return response;
    }
}