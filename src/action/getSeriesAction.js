import {
    GET_SERIES_START,
    GET_SERIES_SUCCESS,
    GET_SERIES_FAIL
} from './constant.js';

import { getDataFromApi } from '../api/getDataFromApi';

const dataFromApi = new getDataFromApi();

export const getSeriesAction = (date) => {
    return async dispatch => {
        dispatch({
            type: GET_SERIES_START,
            statusLoad: {
                isLoad: true,
                isFail: false
            }
        })
        console.log(date)

        try {
            const res = await dataFromApi.getData(date);

            console.log(res)

            dispatch({
                type: GET_SERIES_SUCCESS,
                statusLoad: {
                    isLoad: false,
                    isFail: false
                },
                res
            })
        } catch (error) {
            console.log(error)
            dispatch({
                type: GET_SERIES_FAIL,
                statusLoad: {
                    isLoad: false,
                    isFail: true
                },
                error
            })
        }
    }
}