import React from 'react';
import './App.css';
//react router
import {BrowserRouter as Router, Route} from 'react-router-dom';
//components
import Header from './Header'
import Home from './Home';
import FindSeries from './FindSeries'

class App extends React.Component {
  render() {
    return (
      <Router>
        <Route path ='/' component={Header} />
        <Route exact path='/' component={Home} />
        <Route path='/find-series' component={FindSeries} />
      </Router>
    )
  }
}

export default App;
